$(function() {
	var theme=plat.Data.getValue("THEME");
	$("#themes").combobox("setValue",theme);

	// 加载菜单
	$.post(plat.fullUrl("/base/menucontroller/query.do"), function(data) {
		if (data.menus) {
			$('#index_menu').tree("loadData", data.menus);
			$('#index_menu').tree("collapseAll");
		}
	});
	$('#index_menu').tree({
		onClick : index_newtab
	});
});

/**
 * 点击菜单响应
 * 
 * @param node
 */
function index_newtab(node) {
	var isleaf = $('#index_menu').tree("isLeaf", node.target);
	if (isleaf) {
		if (!node.attributes.url) {
			$.messager.show({
				title : '提示',
				msg : "菜单URL为空！",
				timeout : 2000,
				showType : 'show'
			});
			return;
		}
		var id = "tab_" + node.id.replace("menu_", "");
		var tab = $("#" + id);
		if (tab.length > 0) {
			var index = $("#index_tabs").tabs('getTabIndex', tab);
			$("#index_tabs").tabs("select", index);
		} else {
			//iframe的src属性先为空，然后再赋值，解决iframe下页面请求两遍问题
			//http://fruithardcandy.iteye.com/blog/1884749
			$('#index_tabs').tabs('add',{
					id : id,
					title : node.text,
					content : "<iframe id=" + node.text
							+ " frameborder='0' src='"+plat.fullUrl(node.attributes.url)+"'></iframe>",
					closable : true
				});
//			$(window.frames[node.text]).attr("src",plat.fullUrl(node.attributes.url)); 

		}
	} else {
		if (node.state == "closed") {
			$('#index_menu').tree("expand", node.target);
		} else {
			$('#index_menu').tree("collapse", node.target);
		}
	}
}

function index_closetab(id) {
	var tabs = $('#index_tabs').tabs('close', id);
}

function changeTheme(record) {
	plat.Data.saveData({THEME:record.code},function(){
			window.location.href=window.location.href; 
			window.location.reload; 
		});
}

function logout(){
	// 加载菜单
	$.post(plat.fullUrl("/base/userlogin/logout.do"), function(data) {
			window.location.href=window.location.href; 
			window.location.reload(); 
	});
}
