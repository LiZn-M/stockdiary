package com.cch.stock.set.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.cch.platform.util.DateUtil;
import com.cch.stock.set.bean.SdStock;
import com.cch.stock.set.bean.SdStockDaliy;

public class GetDataFromYahoo {
	
	private final String YAHOO_FINANCE_URL = "http://table.finance.yahoo.com/table.csv";

	/**
	 *  历史数据，不含今天数据
	 * @param stock
	 * @param from
	 * @param to
	 * @return
	 * @throws Exception
	 */
	public ArrayList<SdStockDaliy> getStockCsvData(SdStock stock, Date from, Date to){
		String url=this.getUrl(stock, from, to);
		System.out.println(url);
		ArrayList<SdStockDaliy> list = new ArrayList<SdStockDaliy>();
		
		URL MyURL = null;
		URLConnection con = null;
		InputStreamReader ins = null;
		BufferedReader in = null;
		try {
			MyURL = new URL(url);
			con = MyURL.openConnection();
			ins = new InputStreamReader(con.getInputStream(), "UTF-8");
			in = new BufferedReader(ins);
			// 标题行
			String newLine = in.readLine();
			while ((newLine = in.readLine()) != null) {
				list.add(this.createObject(stock, newLine));
			}
		}catch(Exception e){ 
			e.printStackTrace();
		}finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return list;
	}

	
	/**
	 * ===================================================
	 * 创建url
	 * ===================================================
	 * 参数
	 *  s – 股票代码
	 *  a – 起始时间，月
	 *  b – 起始时间，日
	 *  c – 起始时间，年
	 *  d – 结束时间，月
	 *  e – 结束时间，日
	 *  f – 结束时间，年
	 *  g – 时间周期。Example: g=w, 表示周期是’周’。d->’日’(day), w->’周’(week)，m->’月’(mouth)，v->’dividends only’
	 */
	private String getUrl(SdStock stock,Date from, Date to){
		Calendar calFrom=DateUtil.parseCalendar(from);
		Calendar calTo=DateUtil.parseCalendar(to);
		
		StringBuffer url = new StringBuffer(YAHOO_FINANCE_URL);
		url.append("?s=" + stock.getStockCode()+"."+stock.getStockMarket()); 
		url.append("&a=").append(calFrom.get(Calendar.MONTH));
		url.append("&b=").append(calFrom.get(Calendar.DAY_OF_MONTH));
		url.append("&c=").append(calFrom.get(Calendar.YEAR));
		url.append("&d=").append(calTo.get(Calendar.MONTH));
		url.append("&e=").append(calTo.get(Calendar.DAY_OF_MONTH));
		url.append("&f=").append(calTo.get(Calendar.YEAR));
		url.append("&g=d&ignore=.csv");
		return url.toString();
	}
	
	/**
	 * Date,Open,High,Low,Close,Volume,Adj Close
	 * @param line
	 * @return
	 * @throws ParseException 
	 */
	private SdStockDaliy createObject(SdStock stock, String line) throws ParseException {
		SdStockDaliy sd=new SdStockDaliy();
		String[] data = line.trim().split(",");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		sd.setStockCode(stock.getStockCode());
		sd.setReDate(sdf.parse(data[0]));
		sd.setPriceOpen(Double.parseDouble(data[1]));
		sd.setPriceHigh(Double.parseDouble(data[2]));
		sd.setpriceLow(Double.parseDouble(data[3]));
		sd.setVolume(Long.parseLong(data[5]));
		sd.setPriceClose(Double.parseDouble(data[6]));
		return sd;
	}
}