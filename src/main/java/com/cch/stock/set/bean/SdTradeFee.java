package com.cch.stock.set.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * SdTradeFee entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "sd_trade_fee")
public class SdTradeFee implements java.io.Serializable {

	// Fields

	private Integer feeId;
	private Integer userId;
	private String tradeCode;
	private String stockMarket;
	private String feeCommisionBase;
	private Double feeCommisionRate;
	private Double feeCommisionMin;
	private Double feeCommisionMax;
	private String feeTaxBase;
	private Double feeTaxRate;
	private Double feeTaxMin;
	private Double feeTaxMax;
	private String feeOtherBase;
	private Double feeOtherRate;
	private Double feeOtherMin;
	private Double feeOtherMax;

	// Constructors

	/** default constructor */
	public SdTradeFee() {
	}

	/** full constructor */
	public SdTradeFee(Integer userId, String tradeCode, String stockMarket,
			String feeCommisionBase, Double feeCommisionRate,
			Double feeCommisionMin, Double feeCommisionMax, String feeTaxBase,
			Double feeTaxRate, Double feeTaxMin, Double feeTaxMax,
			String feeOtherBase, Double feeOtherRate, Double feeOtherMin,
			Double feeOtherMax) {
		this.userId = userId;
		this.tradeCode = tradeCode;
		this.stockMarket = stockMarket;
		this.feeCommisionBase = feeCommisionBase;
		this.feeCommisionRate = feeCommisionRate;
		this.feeCommisionMin = feeCommisionMin;
		this.feeCommisionMax = feeCommisionMax;
		this.feeTaxBase = feeTaxBase;
		this.feeTaxRate = feeTaxRate;
		this.feeTaxMin = feeTaxMin;
		this.feeTaxMax = feeTaxMax;
		this.feeOtherBase = feeOtherBase;
		this.feeOtherRate = feeOtherRate;
		this.feeOtherMin = feeOtherMin;
		this.feeOtherMax = feeOtherMax;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "fee_id", unique = true, nullable = false)
	public Integer getFeeId() {
		return this.feeId;
	}

	public void setFeeId(Integer feeId) {
		this.feeId = feeId;
	}

	@Column(name = "user_id")
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "trade_code", length = 100)
	public String getTradeCode() {
		return this.tradeCode;
	}

	public void setTradeCode(String tradeCode) {
		this.tradeCode = tradeCode;
	}

	@Column(name = "stock_market", length = 100)
	public String getStockMarket() {
		return this.stockMarket;
	}

	public void setStockMarket(String stockMarket) {
		this.stockMarket = stockMarket;
	}

	@Column(name = "fee_commision_base", length = 100)
	public String getFeeCommisionBase() {
		return this.feeCommisionBase;
	}

	public void setFeeCommisionBase(String feeCommisionBase) {
		this.feeCommisionBase = feeCommisionBase;
	}

	@Column(name = "fee_commision_rate", precision = 16, scale = 4)
	public Double getFeeCommisionRate() {
		return this.feeCommisionRate;
	}

	public void setFeeCommisionRate(Double feeCommisionRate) {
		this.feeCommisionRate = feeCommisionRate;
	}

	@Column(name = "fee_commision_min", precision = 16, scale = 4)
	public Double getFeeCommisionMin() {
		return this.feeCommisionMin;
	}

	public void setFeeCommisionMin(Double feeCommisionMin) {
		this.feeCommisionMin = feeCommisionMin;
	}

	@Column(name = "fee_commision_max", precision = 16, scale = 4)
	public Double getFeeCommisionMax() {
		return this.feeCommisionMax;
	}

	public void setFeeCommisionMax(Double feeCommisionMax) {
		this.feeCommisionMax = feeCommisionMax;
	}

	@Column(name = "fee_tax_base", length = 100)
	public String getFeeTaxBase() {
		return this.feeTaxBase;
	}

	public void setFeeTaxBase(String feeTaxBase) {
		this.feeTaxBase = feeTaxBase;
	}

	@Column(name = "fee_tax_rate", precision = 16, scale = 4)
	public Double getFeeTaxRate() {
		return this.feeTaxRate;
	}

	public void setFeeTaxRate(Double feeTaxRate) {
		this.feeTaxRate = feeTaxRate;
	}

	@Column(name = "fee_tax_min", precision = 16, scale = 4)
	public Double getFeeTaxMin() {
		return this.feeTaxMin;
	}

	public void setFeeTaxMin(Double feeTaxMin) {
		this.feeTaxMin = feeTaxMin;
	}

	@Column(name = "fee_tax_max", precision = 16, scale = 4)
	public Double getFeeTaxMax() {
		return this.feeTaxMax;
	}

	public void setFeeTaxMax(Double feeTaxMax) {
		this.feeTaxMax = feeTaxMax;
	}

	@Column(name = "fee_other_base", length = 100)
	public String getFeeOtherBase() {
		return this.feeOtherBase;
	}

	public void setFeeOtherBase(String feeOtherBase) {
		this.feeOtherBase = feeOtherBase;
	}

	@Column(name = "fee_other_rate", precision = 16, scale = 4)
	public Double getFeeOtherRate() {
		return this.feeOtherRate;
	}

	public void setFeeOtherRate(Double feeOtherRate) {
		this.feeOtherRate = feeOtherRate;
	}

	@Column(name = "fee_other_min", precision = 16, scale = 4)
	public Double getFeeOtherMin() {
		return this.feeOtherMin;
	}

	public void setFeeOtherMin(Double feeOtherMin) {
		this.feeOtherMin = feeOtherMin;
	}

	@Column(name = "fee_other_max", precision = 16, scale = 4)
	public Double getFeeOtherMax() {
		return this.feeOtherMax;
	}

	public void setFeeOtherMax(Double feeOtherMax) {
		this.feeOtherMax = feeOtherMax;
	}

}