package com.cch.platform.fiter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cch.platform.base.bean.BaseUser;

public class InitParamsFilter implements Filter{

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest hrequest=(HttpServletRequest) request;
		String path=hrequest.getServletPath();
		if("/base/login.jsp".equals(path) || "/base/loginCheck.do".equals(path)){
			chain.doFilter(request, response);
			return;
		}
		Object user=hrequest.getSession().getAttribute("user");
		if(user!=null && user instanceof BaseUser){
			chain.doFilter(request, response);
		}else{
			HttpServletResponse hresponse=(HttpServletResponse) response;
			hresponse.sendRedirect(hrequest.getContextPath()+"/base/login.jsp");
		}
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
