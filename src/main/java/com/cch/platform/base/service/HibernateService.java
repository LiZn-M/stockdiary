package com.cch.platform.base.service;

import java.io.File;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.HibernateTransactionManager;
import org.springframework.stereotype.Service;

@Service
public class HibernateService{
	@Autowired
	private HibernateTemplate ht=null;
	@Autowired
	private HibernateTransactionManager  htm=null;

	public void hibernateBuffer(){

		
	}
	
	public void reloadConfig(){
		Configuration config=new Configuration();
		config.configure("../config/hibernate.cfg.xml");
		SessionFactory sessionFactory=config.buildSessionFactory();
		ht.setSessionFactory(sessionFactory);
		htm.setSessionFactory(sessionFactory);
	}
}
