package com.cch.test.generic;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class GericTestEx extends GericTest<String> {
	

	@SuppressWarnings("unchecked")
	public GericTestEx(){
		Type genType = getClass().getGenericSuperclass();
		Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
		entityClass = (Class) params[0];
		System.out.println(entityClass);
	}
	
	public static void main(String[] args) {
		new GericTestEx();
	}
}
